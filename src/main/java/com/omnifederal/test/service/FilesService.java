package com.omnifederal.test.service;

import feign.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;

@Service
@RequestMapping("/api/files")
public class FilesService {
    @Value("${testapp.filesDir}")
    String filesDir;

    private static final Logger log = LoggerFactory.getLogger(FilesService.class);

    @PostMapping(
        path="/{name}",
        consumes="application/octet-stream")
    @ResponseBody
    public void files(@RequestHeader HttpHeaders headers,
                      @PathVariable("name") String name,
                      @RequestBody byte[] rawData) throws IOException {
        byte data[] = rawData;
        if (headers.containsKey("Content-Encoding")) {
            if (headers.get("Content-Encoding").contains("base64")) {
                data = Base64.getDecoder().decode(rawData);
            }
        }
        Files.write(Paths.get(filesDir).resolve(name), data, StandardOpenOption.CREATE);
    }

    Stream<Path> streamFiles() throws IOException {
        Path rootLocation = Paths.get(filesDir);
        return Files.walk(rootLocation,3)
            .filter( p -> p.toFile().isFile() )
            .map(path -> rootLocation.relativize(path));
    }

    @GetMapping(path="", produces = "application/json")
    @ResponseBody
    public List<String> files() {
        try {
            List<String> filenames = streamFiles().map( path -> { return path.toFile().toString(); })
                .collect(Collectors.toList());
            log.info("Returned {}", filenames);
            return filenames;
        }
        catch (Exception e) {
            log.warn("Unable to list files at {} error {}", filesDir, e.getLocalizedMessage());
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping(path="/{name}", produces="application/octet-stream",
                headers={ "Content-Encoding", "base64",
                          "Transfer-Encoding", "base64" } )
    @ResponseBody
    public String file(@PathVariable("name") String name) throws IOException {
       byte data[] = Files.readAllBytes(Paths.get(filesDir).resolve(name));
       return Base64.getEncoder().encodeToString(data);
    }
}
